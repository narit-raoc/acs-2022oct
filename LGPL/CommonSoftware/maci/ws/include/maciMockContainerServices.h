#ifndef INCLUDE_MOCK_CONTAINER_SERVICE_H_
#define INCLUDE_MOCK_CONTAINER_SERVICE_H_
#include <acscomponentImpl.h>
#include <acsContainerServices.h>

//template<class TObj, class TImpl>
//class AdvancedObject : public virtual TObj, public virtual TImpl {
//  public:
//    AdvancedObject(const ACE_CString& name, maci::ContainerServices* containerServices) : TImpl(name, containerServices), acscomponent::ACSComponentImpl(name, containerServices) {}
//};

class MockContainerServices : public maci::ContainerServices {
  public:
    MockContainerServices(ACE_CString& componentName, PortableServer::POA_ptr poa);
    ~MockContainerServices();    
    virtual CORBA::Object* getCORBAComponent(const char* name);
    virtual CORBA::Object* getCORBAComponentNonSticky(const char* name);
    virtual CORBA::Object* getCORBADynamicComponent(maci::ComponentSpec compSpec, bool markAsDefault);
    virtual CORBA::Object* getCORBACollocatedComponent(maci::ComponentSpec compSpec, bool markAsDefault, const char* targetComponent);
    virtual CORBA::Object* getCORBADefaultComponent(const char* idlType);
    virtual maci::ComponentInfo getComponentDescriptor(const char* componentName);
    virtual ACE_CString_Vector findComponents(const char *nameWilcard, const char *typeWildcard);
    virtual void releaseComponent(const char *name);
    virtual void releaseAllComponents();
    virtual CDB::DAL_ptr getCDB();
    virtual PortableServer::POA_var getOffShootPOA();
    virtual ACS::OffShoot_ptr activateOffShoot(PortableServer::Servant cbServant);
    virtual void deactivateOffShoot(PortableServer::Servant cbServant);
    virtual PortableServer::POA_var createOffShootPOA();
    virtual maci::ComponentStateManager* getComponentStateManager();
    virtual acsalarm::AlarmSource* getAlarmSource();
    //template<class TObj, class TImpl> void activateComponent(const char* name) {
    //    this->comps[name] = new AdvancedObject<TObj, TImpl>(name, this);
    //};
    template<class TAdvancedObject> void activateComponent(const char* name) {
        this->comps[name] = new TAdvancedObject(name, this);
    };
    virtual deactivateComponent(const char* name) {
	this->comps.erase(name);
    }
  protected:
    std::map<std::string, CORBA::Object_var> comps;
};
#endif

