#include "maciMockContainerServices.h"
#include "maciTestClassImpl.h"

//template<>
//class AdvancedObject<MACI_TEST::MaciTestClass, MaciTestClass> : public virtual MACI_TEST::MaciTestClass, public virtual MaciTestClass {
//  public:
//    AdvancedObject(const ACE_CString& name, maci::ContainerServices* containerServices) : MaciTestClass::MaciTestClass(name, containerServices), acscomponent::ACSComponentImpl(name, containerServices) {}
//    virtual CORBA::Boolean test() {return MaciTestClass::test();}
//};

class MaciTestClassObject : public virtual MACI_TEST::MaciTestClass, public virtual MaciTestClass {
  public:
    MaciTestClassObject(const ACE_CString& name, maci::ContainerServices* containerServices) : MaciTestClass::MaciTestClass(name, containerServices), acscomponent::ACSComponentImpl(name, containerServices) {}
    virtual CORBA::Boolean test() {return MaciTestClass::test();}
};

int main() {
    ACE_CString cn("Container");
    MockContainerServices* mcs = new MockContainerServices(cn, nullptr);
    //mcs->activateComponent<MACI_TEST::MaciTestClass, MaciTestClass>("Component");
    //mcs->activateComponent<MACI_TEST::MaciTestClass, MaciTestClass>("TEST");
    mcs->activateComponent<MaciTestClassObject>("Component");
    mcs->activateComponent<MaciTestClassObject>("TEST");

    maci::ContainerServices* cs = static_cast<maci::ContainerServices*>(mcs);
    MACI_TEST::MaciTestClass_var comp = cs->getComponent<MACI_TEST::MaciTestClass>("Component");
    comp->test();
    return 0;
}
