#include "maciMockContainerServices.h"

MockContainerServices::MockContainerServices(ACE_CString& componentName, PortableServer::POA_ptr poa) :
maci::ContainerServices(componentName, poa) {
}
MockContainerServices::~MockContainerServices(){
}
CORBA::Object* MockContainerServices::getCORBAComponent(const char* name){
    if (comps.find(name) == comps.end()) {
        maciErrType::CannotGetComponentExImpl ex(__FILE__, __LINE__, "MockContainerServices::getComponent");
        ex.setCURL(name);
        throw ex;
    }
    return CORBA::Object::_duplicate(comps[name].in());
}
CORBA::Object* MockContainerServices::getCORBAComponentNonSticky(const char* name){
    return nullptr;
}
CORBA::Object* MockContainerServices::getCORBADynamicComponent(maci::ComponentSpec compSpec, bool markAsDefault){
    return nullptr;
}
CORBA::Object* MockContainerServices::getCORBACollocatedComponent(maci::ComponentSpec compSpec, bool markAsDefault, const char* targetComponent){
    return nullptr;
}
CORBA::Object* MockContainerServices::getCORBADefaultComponent(const char* idlType){
    return nullptr;
}
maci::ComponentInfo MockContainerServices::getComponentDescriptor(const char* componentName){
    return maci::ComponentInfo();
}
ACE_CString_Vector MockContainerServices::findComponents(const char *nameWilcard, const char *typeWildcard){
    return ACE_CString_Vector();
}
void MockContainerServices::releaseComponent(const char *name){
}
void MockContainerServices::releaseAllComponents(){
}
CDB::DAL_ptr MockContainerServices::getCDB(){
    return nullptr;
}
PortableServer::POA_var MockContainerServices::getOffShootPOA(){
    return PortableServer::POA_var();
}
ACS::OffShoot_ptr MockContainerServices::activateOffShoot(PortableServer::Servant cbServant){
    return nullptr;
}
void MockContainerServices::deactivateOffShoot(PortableServer::Servant cbServant){
}
PortableServer::POA_var MockContainerServices::createOffShootPOA(){
    return getOffShootPOA();
}
maci::ComponentStateManager* MockContainerServices::getComponentStateManager(){
    return nullptr;
}
acsalarm::AlarmSource* MockContainerServices::getAlarmSource(){
    return nullptr;
}
/*
Logging::Loggable getLogger(){
    return Logging::Loggable();
}*/
