#!/usr/bin/env bash

program="${0##*/}"
PATH=${PATH#*:}

#Generate coverage raw data
if [ -n "$ACS_COVERAGE" ]; then
    #Define Python site-packages according to ACSROOT/INTLIST/INTROOT
    if [ -n "$ACSROOT" ]; then
        SPATH="$ACSROOT/${PYTHON_SITE_PACKAGES}"
    fi
    if [ -n "$INTLIST" ]; then
        for int in ${INTLIST//:/ }; do
            SPATH="$int/${PYTHON_SITE_PACKAGES},$SPATH"
        done
    fi
    if [ -n "$INTROOT" ]; then
        SPATH="$INTROOT/${PYTHON_SITE_PACKAGES},$SPATH"
    fi

    #Find .coveragerc on ACS installation areas
    RCFILE=$(searchFile config/.coveragerc)
    [[ ! "$RCFILE" == "#error#" ]] && RCFILE="--rcfile $RCFILE/config/.coveragerc" || RCFILE=

    #Run appropriate wrapped coverage command
    if [ "$program" == "nose2" ]; then
        coverage run $RCFILE --source $SPATH -p -m $program "$@"
    elif [ "$program" == "python" ]; then
        if [ "$1" == "-V" ]; then
            "$program" "$@"
        else
            if [ "$1" == "-c" ]; then
                script=/tmp/coverage_cmd.$RANDOM.py
                echo "$2" > $script
                shift 2
                set -- $script "$@"
            fi
            coverage run $RCFILE --source $SPATH -p "$@"
        fi
    else
        echo "Unhandled program '$program' called with parameters: '$@'"
    fi
else
    "$program" "$@"
fi
